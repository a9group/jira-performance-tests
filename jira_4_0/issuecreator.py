from HTTPClient import NVPair
from env import request, extract, valueOrEmpty, valueOrDefault, postMultipart
from java.util.regex import Pattern

class IssueCreator:
    
    def __init__(self, testIndex):
        
        self.requests = {
            'create' : request(testIndex, 'HTTP-REQ : create issue'),
            'view' : request(testIndex + 1, 'HTTP-REQ : view created issue')
        }
        self.patterns = {
            'project_avatar' : Pattern.compile('id="project-avatar".*?src="(.*?)"'),
            'user_avatars' : Pattern.compile('background-image:url\((\/secure\/useravatar?.*?)\)'),
            'create_response_issuekey' : Pattern.compile('browse\/(.*)')
        }        
    
    def create(self, projectId, create={}, cached=False):
        req = self.requests['create']

        req.GET('/secure/CreateIssue.jspa?pid=' + projectId + '&issuetype=' + valueOrDefault(create, 'issuetype', '1'))
        req.GET('/rest/api/1.0/admin/issuetypeschemes?includeRecent=true&_=1328588320472')
        
        if not cached:
            req.GET('/s/472/1/1/_/styles/global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:calendar/jira.webresources:calendar.js')
            req.GET('/s/472/1/1.0/_/download/resources/jira.webresources:calendar/calendar.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/472/1/1/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/icons/priority_critical.gif')
            req.GET('/images/icons/priority_blocker.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/icons/priority_trivial.gif')
            req.GET('/images/icons/priority_minor.gif')
            req.GET('/images/icons/cal.gif')
            req.GET('/images/icons/help_blue.gif')
            req.GET('/images/icons/filter_public.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/s/472/1/_/styles/combined-printable.css')
                
        response = postMultipart(req, '/secure/CreateIssueDetails.jspa', 
        (
            NVPair('summary', valueOrEmpty(create, 'summary')),
            NVPair('priority', valueOrDefault(create, 'priority', '3')),
            NVPair('duedate', valueOrEmpty(create, 'duedate')),
            NVPair('assignee', valueOrDefault(create, 'assignee', '-1')),
            NVPair('environment', valueOrEmpty(create, 'environment')),
            NVPair('description', valueOrEmpty(create, 'description')),
            NVPair('attachment_field.1', valueOrEmpty(create, 'attachment_field.1')),
            NVPair('attachment_field.2', valueOrEmpty(create, 'attachment_field.2')),
            NVPair('attachment_field.3', valueOrEmpty(create, 'attachment_field.3')),
            NVPair('pid', projectId),
            NVPair('issuetype', valueOrDefault(create, 'issuetype', '1')),
            NVPair('Create', 'Create'),
            NVPair('reporter', create['reporter']),
        ))
        
        newIssueKey = extract(response.getHeader('Location'), self.patterns['create_response_issuekey'])
        
        req = self.requests['view']
        req.GET('/browse/'  + newIssueKey)
        
        if not cached:
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/icons/aui-icon-tools.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/0/_/styles/global.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/_/includes/js/pincomment.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')                
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/472/1/0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/bluepixel.gif')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/icons/bullet_creme.gif')
            req.GET('/images/icons/arrow_down_blue_small.gif')
            req.GET('/images/icons/undo_16.gif')
            req.GET('/images/icons/link_out_bot.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/s/472/1/_/styles/combined-printable.css')        
        
        return newIssueKey 

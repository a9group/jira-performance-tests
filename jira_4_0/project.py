from env import request, extract
from java.util.regex import Pattern

class Project:
    
    def __init__(self, testIndex, projectKey, projectId):
        self.projectKey = projectKey
        self.projectId = projectId
        self.requests = {
            'view' : request(testIndex, 'HTTP-REQ : view project')
        }
        self.patterns = {
            'chart' : Pattern.compile('<img src=\'(\/charts\?filename=.*?)\''),
            'streams' : Pattern.compile('id="gadget-stream.*?src="http:\/\/.*?(\/.*?)"'),
            'project_avatar' : Pattern.compile('id="project-avatar".*?src="(.*?)"')
        }
        
    def getProjectKey(self):
        return self.projectKey
    
    def getProjectId(self):
        return self.projectId
        
    def view(self, cached=False):
        req = self.requests['view']
        page = req.GET('/browse/' + self.projectKey).text
        
        req.GET(extract(page, self.patterns['chart']))
        req.GET(extract(page, self.patterns['streams']))
        req.GET('/s/472/1/2/_/download/resources/com.atlassian.streams.streams-jira-plugin:date-en-US/date-en-US.js?_=1328152149724')
        req.GET('/rest/activity-stream/1.0/url?_=1328152149726&keys=' + self.projectKey + '&numofentries=10&username=')
        req.GET('/plugins/servlet/streams?key=' + self.projectKey + '&os_authType=basic&maxResults=10&_=1328152149793')

        if not cached:
            req.GET(extract(page, self.patterns['project_avatar']))        
            req.GET('/s/472/1/0/_/styles/global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:ajaxhistory/jira.webresources:ajaxhistory.js')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:browseproject/jira.webresources:browseproject.js')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/472/1/0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/icons/newfeature.gif')
            req.GET('/s/472/1/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/task.gif')
            req.GET('/images/icons/improvement.gif')
            req.GET('/s/472/1/_/images/icons/ico_reports.png')
            req.GET('/s/472/1/_/images/icons/ico_filters.png')
            req.GET('/images/icons/box_16.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/4.0.2/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.js')
            req.GET('/s/472/1/4.0.2/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.css')
            req.GET('/s/472/1/3.0.14/_/download/batch/com.atlassian.streams.streams-jira-plugin:streamsWebResources/com.atlassian.streams.streams-jira-plugin:streamsWebResources.js')
            req.GET('/s/472/1/3.0.14/_/download/batch/com.atlassian.streams.streams-jira-plugin:streamsWebResources/com.atlassian.streams.streams-jira-plugin:streamsWebResources.css')
            req.GET('/s/472/1/4.0.2/_/images/gadgets/loading.gif')
            req.GET('/s/472/1/_/styles/combined-printable.css')
            req.GET('/s/472/1/3.0.14/_/download/batch/com.atlassian.streams.streams-jira-plugin:streamsWebResources/images/feedicon.png')
            req.GET('/s/472/1/3.0.14/_/download/batch/com.atlassian.streams.streams-jira-plugin:streamsWebResources/images/showcomment.png')

    def viewTabVersions(self, cached=False):
        req = self.requests['view']
        
        page = req.GET('/browse/' + self.projectKey + '#selectedTab=com.atlassian.jira.plugin.system.project%3Aversions-panel').text
        
        if not cached:
            req.GET(extract(page, self.patterns['project_avatar']))
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/0/_/styles/global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:ajaxhistory/jira.webresources:ajaxhistory.js')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:browseproject/jira.webresources:browseproject.js')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/472/1/0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/newfeature.gif')
            req.GET('/s/472/1/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/icons/improvement.gif')
            req.GET('/images/icons/task.gif')
            req.GET('/images/icons/box_16.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/_/styles/combined-printable.css')
            
    def viewTabIssues(self, cached=False):
        req = self.requests['view']
        
        page = req.GET('/browse/' + self.projectKey + '#selectedTab=com.atlassian.jira.plugin.system.project%3Aissues-panel').text
        
        if not cached:
            req.GET(extract(page, self.patterns['project_avatar']))
            req.GET('/s/472/1/0/_/styles/global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:ajaxhistory/jira.webresources:ajaxhistory.js')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:browseproject/jira.webresources:browseproject.js')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/472/1/0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/icons/bug.gif')
            req.GET('/s/472/1/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/icons/newfeature.gif')
            req.GET('/images/icons/task.gif')
            req.GET('/images/icons/improvement.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/_/styles/combined-printable.css')

    def viewTabComponents(self, cached=False):
        req = self.requests['view']
        
        page = req.GET('/browse/' + self.projectKey + '#selectedTab=com.atlassian.jira.plugin.system.project%3Acomponents-panel').text
        
        if not cached:
            req.GET(extract(page, self.patterns['project_avatar']))
            req.GET('/s/472/1/0/_/styles/global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:ajaxhistory/jira.webresources:ajaxhistory.js')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.1.2/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:browseproject/jira.webresources:browseproject.js')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/472/1/0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/newfeature.gif')
            req.GET('/s/472/1/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/icons/task.gif')
            req.GET('/images/icons/improvement.gif')
            req.GET('/images/icons/component.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/_/styles/combined-printable.css')
        
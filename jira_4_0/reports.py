from env import request, extract
from java.util.regex import Pattern

class Reports:
    
    def __init__(self, testId):
        self.requests = {
            'configure' : request(testId, 'HTTP REQ : configure report'),
            'report' : request(testId + 1, 'HTTP REQ : view report')
        }
        self.patterns = {
            'chart' : Pattern.compile('(\/charts\?filename=jfreechart-onetime-.*?\.png)')
        }
        
    def viewCreatedVsResolved(self, projectId, cached=False):
        req = self.requests['configure']
        
        req.GET('/secure/ConfigureReport!default.jspa?selectedProjectId=' + projectId + '&reportKey=com.atlassian.jira.plugin.system.reports:createdvsresolved-report')
                
        if not cached:
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1/_/styles/global.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/s/472/1/_/styles/combined-printable.css')
                
        req.GET('/secure/FilterPickerPopup.jspa?showProjects=true&field=projectOrFilterId')
        
        if not cached:
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.js')
            req.GET('/images/border/spacer.gif')
            req.GET('/images/jira111x30.png')
                
        req.GET('/secure/FilterPickerPopup.jspa?showProjects=true&filterView=projects&field=projectOrFilterId')
                
        req = self.requests['report']
        page = req.GET('/secure/ConfigureReport.jspa?projectOrFilterId=project-' + projectId + '&periodName=daily&daysprevious=30&cumulative=true&versionLabels=major&selectedProjectId=' + projectId + '&reportKey=com.atlassian.jira.plugin.system.reports:createdvsresolved-report&Next=Next').text
        req.GET(extract(page, self.patterns['chart']))
        
        
    def viewRecentlyCreatedIssues(self, projectId, cached=False):
        req = self.requests['configure']
        
        req.GET('/secure/ConfigureReport!default.jspa?selectedProjectId=' + projectId + '&reportKey=com.atlassian.jira.plugin.system.reports:recentlycreated-report')
        
        if not cached:
            req.GET('/s/472/1/1/_/styles/global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/s/472/1/_/styles/combined-printable.css')

        req.GET('/secure/FilterPickerPopup.jspa?showProjects=true&field=projectOrFilterId')
        
        if not cached:
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.js')
            req.GET('/images/border/spacer.gif')
            req.GET('/images/jira111x30.png')
                
        req.GET('/secure/FilterPickerPopup.jspa?showProjects=true&filterView=projects&field=projectOrFilterId')
                
        req = self.requests['report']
        page = req.GET('/secure/ConfigureReport.jspa?projectOrFilterId=project-' + projectId + '&periodName=daily&daysprevious=30&selectedProjectId=' + projectId + '&reportKey=com.atlassian.jira.plugin.system.reports:recentlycreated-report&Next=Next').text
        req.GET(extract(page, self.patterns['chart']))
        
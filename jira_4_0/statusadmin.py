from HTTPClient import NVPair
from env import request, extract, valueOrDefault, valueOrEmpty
from java.util.regex import Pattern

class StatusAdmin:
    
    def __init__(self, testId):
        self.requests = {
            'browse' : request(testId, 'HTTP-REQ : view statuses'),
            'add' : request(testId + 1, 'HTTP-REQ : add status'),
            'edit' : request(testId + 2, 'HTTP-REQ : edit status')
        }
        self.patterns = {
            'edit_name' : Pattern.compile('(?s)name="name".*?value="(.*?)"'),
            'edit_description' : Pattern.compile('(?s)name="description".*?value="(.*?)"'),
            'edit_iconurl' : Pattern.compile('(?s)name="iconurl".*?value="(.*?)"')
        }
        
    
    def browse(self, cached=False):
        req = self.requests['browse']
        
        req.GET('/secure/admin/ViewStatuses.jspa')
        
        if not cached:
            req.GET('/s/472/1/9/_/styles/global.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/_/includes/js/adminMenu.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/472/1/9/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/472/1/_/images/icons/navigate_down_10.gif')
            req.GET('/s/472/1/_/images/icons/navigate_right_10.gif')
            req.GET('/images/border/spacer.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/images/icons/bullet_blue.gif')
            req.GET('/images/icons/bullet_creme.gif')
            req.GET('/images/icons/status_inprogress.gif')
            req.GET('/images/icons/status_reopened.gif')
            req.GET('/images/icons/status_resolved.gif')
            req.GET('/images/icons/status_closed.gif')
            req.GET('/s/472/1/_/images/icons/help_blue.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/s/472/1/_/styles/combined-printable.css')
            
        
    def add(self, status, cached=False):
        self.requests['add'].POST('/secure/admin/AddStatus.jspa',
        (
            NVPair('name', status['name']),
            NVPair('description', valueOrEmpty(status, 'description')),
            NVPair('iconurl', valueOrDefault(status, 'iconurl', '/images/icons/status_generic.gif')),
            NVPair('Add', 'Add'),
        ), (
            NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
        ))
                                  
                                  
    def edit(self, status, cached=False):
        req = self.requests['edit']
        sid = status['id']
        
        page = req.GET('/secure/admin/EditStatus!default.jspa?id=' + sid).text
        
        if not cached:
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/9/_/styles/global.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/_/includes/js/adminMenu.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/472/1/9/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/472/1/_/images/icons/navigate_down_10.gif')
            req.GET('/s/472/1/_/images/icons/navigate_right_10.gif')
            req.GET('/images/border/spacer.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/s/472/1/_/styles/combined-printable.css')
                
        req.POST('/secure/admin/EditStatus.jspa',
            (
                NVPair('name', valueOrDefault(status, 'name', extract(page, self.patterns['edit_name']))),
                NVPair('description', valueOrDefault(status, 'description', extract(page, self.patterns['edit_description']))),
                NVPair('iconurl', valueOrDefault(status, 'iconurl', extract(page, self.patterns['edit_iconurl']))),
                NVPair('Update', 'Update'),
                NVPair('id', sid),
            ), ( 
                NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
            ))

        self.browse(cached)

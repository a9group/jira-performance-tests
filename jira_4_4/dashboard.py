from env import request, extract, host
from HTTPClient import NVPair
from java.util.regex import Pattern

class Dashboard:
    
    def __init__(self, testIndex):
        self.requests = {
            'home_not_logged_in' : request(testIndex, 'HTTP-REQ : home not logged in'),
            'login' : request(testIndex + 1, 'HTTP-REQ : perform login'),
            'home_logged_in' : request(testIndex + 2, 'HTTP-REQ : home logged in'),
        }
        self.gadgetRegex = {
            'login' : Pattern.compile('id":"0.*?renderedGadgetUrl":"http:\\\/\\\/.*?(\\\/.*?)"'),
            'intro' : Pattern.compile('id":"10000.*?renderedGadgetUrl":"http:\\\/\\\/.*?(\\\/.*?)"'),
            'stream' : Pattern.compile('id":"10001.*?renderedGadgetUrl":"http:\\\/\\\/.*?(\\\/.*?)"'),
            'assignedToMe' : Pattern.compile('id":"10002.*?renderedGadgetUrl":"http:\\\/\\\/.*?(\\\/.*?)"'),
            'favFilters' : Pattern.compile('id":"10003.*?renderedGadgetUrl":"http:\\\/\\\/.*?(\\\/.*?)"'),
            'admin' : Pattern.compile('id":"10004.*?renderedGadgetUrl":"http:\\\/\\\/.*?(\\\/.*?)"')
        }
        
    # go to JIRA home and log in
    def login(self, username, password, cached=False):
        self.requests['login'].POST('/rest/gadget/1.0/login', ( 
                NVPair('os_username', username), 
                NVPair('os_password', password), 
                NVPair('os_captcha', ''), 
            ), ( 
                NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
            ))
    
    def loggedIn(self, cached=False):
        req = self.requests['home_logged_in']
        
        req.GET('/')
        page = req.GET('/secure/Dashboard.jspa').text
        
        req.GET(extract(page, self.gadgetRegex['stream'], (('\/', '/'), )))
        req.GET(extract(page, self.gadgetRegex['intro'], (('\/', '/'), )))
        req.GET(extract(page, self.gadgetRegex['assignedToMe'], (('\/', '/'), )))
        req.GET(extract(page, self.gadgetRegex['favFilters'], (('\/', '/'), )))
        req.POST('/plugins/servlet/gadgets/dashboard-diagnostics', ( NVPair('uri', host() + '/secure/Dashboard.jspa'), ), ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), ) )
        req.GET('/rest/gadget/1.0/intro')
        req.GET('/rest/gadget/1.0/favfilters?showCounts=true')
        req.GET('/rest/gadget/1.0/issueTable/jql?jql=assignee+=+currentUser()+AND+resolution+=+unresolved+ORDER+BY+priority+DESC,+created+ASC&num=10&addDefault=true&columnNames=&enableSorting=true&sortBy=null&paging=true&startIndex=0&showActions=true')
        # ignore till we fix streams
        # req.GET('/plugins/servlet/streams?maxResults=5&streams=')
        req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/resources/com.atlassian.streams.streams-jira-plugin:date-en-US/date.js?callback=ActivityStreams.loadDateJs&_=1328225961361')
        
        if self.gadgetRegex['admin'].matcher(page).find():
            req.GET('/rest/gadget/1.0/admin')
            req.GET(extract(page, self.gadgetRegex['admin'], (('\/', '/'), )))
        
        if not cached:        
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/images/icons/aui-icon-tools.gif')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets-lite/images/loading.gif')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.dashboard:dashboard/com.atlassian.gadgets.dashboard:dashboard.css')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.dashboard:gadget-dashboard-resources/com.atlassian.gadgets.dashboard:gadget-dashboard-resources.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.dashboard:dashboard/com.atlassian.gadgets.dashboard:dashboard.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.dashboard:gadget-dashboard-resources/com.atlassian.gadgets.dashboard:gadget-dashboard-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js?context=issueaction&context=issuenavigation')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/_/images/jira111x30.png')
            req.GET('/rest/api/1.0/header-separator?color=#f0f0f0&bgcolor=#3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/tools_20.png')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/resources/com.atlassian.gadgets.dashboard:dashboard/css/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/plugins/servlet/gadgets/js/auth-refresh.js?v=61901225c398ca5626e5f70307fcebd&container=atlassian&debug=0')
            req.GET('/s/en_US-64k3hp/664/2/3.4.2/_/download/batch/com.atlassian.auiplugin:ajs-gadgets/com.atlassian.auiplugin:ajs-gadgets.js')
            req.GET('/s/en_US-64k3hp/664/2/3.4.2/_/download/batch/com.atlassian.auiplugin:ajs-gadgets/com.atlassian.auiplugin:ajs-gadgets.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.gadgets:common-lite/com.atlassian.jira.gadgets:common-lite.css')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets-lite/com.atlassian.gadgets.publisher:ajs-gadgets-lite.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:util-lite/jira.webresources:util-lite.css')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.publisher:ajax/com.atlassian.gadgets.publisher:ajax.css')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets-lite/com.atlassian.gadgets.publisher:ajs-gadgets-lite.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.gadgets:common-lite/com.atlassian.jira.gadgets:common-lite.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:util-lite/jira.webresources:util-lite.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.gadgets:g-admin/com.atlassian.jira.gadgets:g-admin.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.publisher:ajax/com.atlassian.gadgets.publisher:ajax.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:inline-layer/jira.webresources:inline-layer.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:issue-table/jira.webresources:issue-table.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.gadgets:g-assigned-to-me/com.atlassian.jira.gadgets:g-assigned-to-me.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:inline-layer/jira.webresources:inline-layer.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:dropdown/jira.webresources:dropdown.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:content-retrievers/jira.webresources:content-retrievers.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:list/jira.webresources:list.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:issue-table/jira.webresources:issue-table.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.gadgets:g-assigned-to-me/com.atlassian.jira.gadgets:g-assigned-to-me.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets/com.atlassian.gadgets.publisher:ajs-gadgets.css')
            req.GET('/s/en_US-64k3hp/664/2/3.4.2/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.css')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams:datepicker/com.atlassian.streams:datepicker.css')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams.actions:commentActionHandlers/com.atlassian.streams.actions:commentActionHandlers.css')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams:streamsWebResources/com.atlassian.streams:streamsWebResources.css')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets/com.atlassian.gadgets.publisher:ajs-gadgets.js')
            req.GET('/s/en_US-64k3hp/664/2/3.4.2/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.js')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams:date-default/com.atlassian.streams:date-default.js')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams:datepicker/com.atlassian.streams:datepicker.js')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams.actions:commentActionHandlers/com.atlassian.streams.actions:commentActionHandlers.js')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams.actions:inlineActionsJs/com.atlassian.streams.actions:inlineActionsJs.js')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams:streamsWebResources/com.atlassian.streams:streamsWebResources.js')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams:streams-parent-js/com.atlassian.streams:streams-parent-js.js')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams:date-js/com.atlassian.streams:date-js.js')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams:streamsWebResources/images/feed-icon.png')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams:streamsWebResources/images/full-view-icon.png')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams:streamsWebResources/images/list-view-icon.png')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams:streamsWebResources/images/gadget-loading.gif')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams:streamsWebResources/images/throbber.gif')
            req.GET('/images/intro-gadget.png')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/icons/bug.gif')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/icon_descending.png')
            req.GET('/secure/useravatar?avatarId=10142&s=48')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/resources/jira.webresources:global-static/wiki-renderer.css')
            req.GET('/images/icons/bug.gif')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/sprites/icons_module.png')
        

    # go to home page, which redirects to dashbaord
    def notLoggedIn(self, cached=False):
        req = self.requests['home_not_logged_in']
        
        req.GET('/')
        page = req.GET('/secure/Dashboard.jspa').text
        
        req.GET('/rest/gadget/1.0/intro')
        req.POST('/plugins/servlet/gadgets/dashboard-diagnostics', ( NVPair('uri', host()+'/secure/Dashboard.jspa'), ), ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), ) )
        req.GET(extract(page, self.gadgetRegex['intro'], (('\/', '/'), ( '&amp;', '&'), )))
        req.GET(extract(page, self.gadgetRegex['login'], (('\/', '/'), ( '&amp;', '&'), )))

        if not cached:        
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.dashboard:dashboard/com.atlassian.gadgets.dashboard:dashboard.css')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.dashboard:gadget-dashboard-resources/com.atlassian.gadgets.dashboard:gadget-dashboard-resources.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.dashboard:dashboard/com.atlassian.gadgets.dashboard:dashboard.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.dashboard:gadget-dashboard-resources/com.atlassian.gadgets.dashboard:gadget-dashboard-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js?context=issueaction&context=issuenavigation')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/en_US-64k3hp/664/2/_/images/jira111x30.png')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/en_US-64k3hp/664/2/3.4.2/_/download/batch/com.atlassian.auiplugin:ajs-gadgets/com.atlassian.auiplugin:ajs-gadgets.css')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.publisher:ajax/com.atlassian.gadgets.publisher:ajax.css')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets-lite/com.atlassian.gadgets.publisher:ajs-gadgets-lite.css')
            req.GET('/plugins/servlet/gadgets/js/auth-refresh.js?v=61901225c398ca5626e5f70307fcebd&container=atlassian&debug=0')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:util-lite/jira.webresources:util-lite.css')
            req.GET('/s/en_US-64k3hp/664/2/3.4.2/_/download/batch/com.atlassian.auiplugin:ajs-gadgets/com.atlassian.auiplugin:ajs-gadgets.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.publisher:ajax/com.atlassian.gadgets.publisher:ajax.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.gadgets:common-lite/com.atlassian.jira.gadgets:common-lite.css')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets-lite/com.atlassian.gadgets.publisher:ajs-gadgets-lite.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:util-lite/jira.webresources:util-lite.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:captcha/jira.webresources:captcha.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:captcha/jira.webresources:captcha.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.gadgets:g-login/com.atlassian.jira.gadgets:g-login.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.gadgets:common-lite/com.atlassian.jira.gadgets:common-lite.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets-lite/images/loading.gif')
            req.GET('/images/intro-gadget.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')

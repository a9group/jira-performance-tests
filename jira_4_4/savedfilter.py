from env import request, extract
from java.util.regex import Pattern

class SavedFilter:
    
    def __init__(self, testIndex):
        self.requests = {
            'manage' : request(testIndex, 'HTTP-REQ : manage saved filters'),
            'use' : request(testIndex + 1, 'HTTP-REQ : use a saved filter')
        }
        self.patterns = {
            'atl_token' : Pattern.compile('id="atlassian-token".*?content="(.*?)"')                         
        }
        
    def manage(self, cached=False):
        req = self.requests['manage']
        
        page = req.GET('/secure/ManageFilters.jspa').text
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.css')
            req.GET('/s/en_US-64k3hp/664/3/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.css')
            req.GET('/s/en_US-64k3hp/664/3/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/3/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.js')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:ajaxhistory/jira.webresources:ajaxhistory.js')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.js')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:ajax-favourite-control/jira.webresources:ajax-favourite-control.js')
            req.GET('/s/en_US-64k3hp/664/3/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:managefilters/jira.webresources:managefilters.js')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:manageshared/jira.webresources:manageshared.js')
            req.GET('/s/en_US-64k3hp/664/3/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js')
            req.GET('/s/en_US-64k3hp/664/3/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/s/en_US-64k3hp/664/3/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/s/en_US-64k3hp/664/3/_/images/jira111x30.png')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/header-separator?color=#f0f0f0&bgcolor=#3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/icons/filter_48.png')
            req.GET('/s/en_US-64k3hp/664/3/_/images/icons/ico_help.png')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/images/icons/star_yellow.gif')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/images/icons/filter_private.gif')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/images/icons/cog-dd.png')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')
        
        return extract(page, self.patterns['atl_token'])


    def use(self, atl_token, savedFilterId, cached=False):
        req = self.requests['use']
        
        req.GET('/secure/IssueNavigator.jspa?mode=hide&atl_token=' + atl_token + '&requestId=' + str(savedFilterId))
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.css')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/3/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/3/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/3/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.js')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:jira-fields/jira.webresources:jira-fields.js')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:calendar/jira.webresources:calendar.js')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            req.GET('/s/en_US-64k3hp/664/3/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/en_US-64k3hp/664/3/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            req.GET('/s/en_US-64k3hp/664/3/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/en_US-64k3hp/664/3/4.4.4/_/download/batch/com.atlassian.jira.gadgets:searchrequestview-charts/com.atlassian.jira.gadgets:searchrequestview-charts.js')
            req.GET('/s/en_US-64k3hp/664/3/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/s/en_US-64k3hp/664/3/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/s/en_US-64k3hp/664/3/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js?context=issueaction&context=issuenavigation')
            req.GET('/s/en_US-64k3hp/664/3/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:ajax-favourite-control/jira.webresources:ajax-favourite-control.js')
            req.GET('/s/en_US-64k3hp/664/3/_/images/jira111x30.png')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/header-separator?color=#f0f0f0&bgcolor=#3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/images/icons/view_20.png')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/images/icons/tools_20.png')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/images/icons/permalink_light_16.png')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/images/icons/icon_filtercollapse.png')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/images/tab_bg.png')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/images/mod_header_bg.png')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/images/icons/star_yellow.gif')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/images/icons/icon_descending.png')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/images/icons/ico_activeissue.png')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/images/icons/status_reopened.gif')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')

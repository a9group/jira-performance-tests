from env import request, extract
from java.util.regex import Pattern

class Project:
    
    def __init__(self, testIndex, projectKey, projectId):
        self.projectKey = projectKey
        self.projectId = projectId
        self.requests = {
            'view' : request(testIndex, 'HTTP-REQ : view project')
        }
        self.patterns = {
            'chart' : Pattern.compile('<img src=\'(\/charts\?filename=.*?)\''),
            'streams' : Pattern.compile('id="gadget-1.*?src="http:\/\/.*?(\/.*?)"'),
            'project_avatar' : Pattern.compile('id="project-avatar".*?src="(.*?)"')
        }
        
    def getProjectKey(self):
        return self.projectKey
        
    def getProjectId(self):
        return self.projectId
        
    def view(self, cached=False):
        req = self.requests['view']
        
        page = req.GET('/browse/TST').text
        req.GET(extract(page, self.patterns['chart']))
        req.GET(extract(page, self.patterns['streams']))
        req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/resources/com.atlassian.streams.streams-jira-plugin:date-en-US/date.js?callback=ActivityStreams.loadDateJs&_=1328507249726')
        req.GET('/plugins/servlet/streams?maxResults=10&streams=key+IS+' + self.projectKey)
        
        if not cached:
            req.GET(extract(page, self.patterns['project_avatar']))
            
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:ajaxhistory/jira.webresources:ajaxhistory.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:browseproject/jira.webresources:browseproject.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/batch.css')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/header-separator?color=#f0f0f0&bgcolor=#3c78b5')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/2/_/images/jira111x30.png')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/newfeature.gif')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/tools_20.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/ico_reports.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/ico_filters.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/mod_header_bg.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/images/icons/box_16.gif')
            req.GET('/plugins/servlet/gadgets/js/auth-refresh.js?v=61901225c398ca5626e5f70307fcebd&container=atlassian&debug=0')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.publisher:ajax/com.atlassian.gadgets.publisher:ajax.css')
            req.GET('/s/en_US-64k3hp/664/2/3.4.2/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:util-lite/jira.webresources:util-lite.css')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets/com.atlassian.gadgets.publisher:ajs-gadgets.css')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams:datepicker/com.atlassian.streams:datepicker.css')
            req.GET('/s/en_US-64k3hp/664/3/4.1.2/_/download/batch/com.atlassian.streams.actions:commentActionHandlers/com.atlassian.streams.actions:commentActionHandlers.css')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams:streamsWebResources/com.atlassian.streams:streamsWebResources.css')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.publisher:ajax/com.atlassian.gadgets.publisher:ajax.js')
            req.GET('/s/en_US-64k3hp/664/2/3.4.2/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets/com.atlassian.gadgets.publisher:ajs-gadgets.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:util-lite/jira.webresources:util-lite.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.js')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams:date-default/com.atlassian.streams:date-default.js')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams:datepicker/com.atlassian.streams:datepicker.js')
            req.GET('/s/en_US-64k3hp/664/3/4.1.2/_/download/batch/com.atlassian.streams.actions:commentActionHandlers/com.atlassian.streams.actions:commentActionHandlers.js')
            req.GET('/s/en_US-64k3hp/664/3/4.1.2/_/download/batch/com.atlassian.streams.actions:inlineActionsJs/com.atlassian.streams.actions:inlineActionsJs.js')
            req.GET('/s/en_US-64k3hp/664/3/4.1.2/_/download/batch/com.atlassian.streams.jira.inlineactions:actionHandlers/com.atlassian.streams.jira.inlineactions:actionHandlers.js')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams:date-js/com.atlassian.streams:date-js.js')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams:streamsWebResources/com.atlassian.streams:streamsWebResources.js')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams:streams-parent-js/com.atlassian.streams:streams-parent-js.js')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams:streamsWebResources/images/gadget-loading.gif')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets/images/tools_12.png')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams:streamsWebResources/images/feed-icon.png')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/en_US-64k3hp/664/2/4.1.2/_/download/batch/com.atlassian.streams:streamsWebResources/images/throbber.gif')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/resources/jira.webresources:global-static/wiki-renderer.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')
            req.GET('/images/icons/bug.gif')


    def viewTabVersions(self, cached=False):
        req = self.requests['view']
        
        page = req.GET('/browse/' + self.projectKey + '#selectedTab=com.atlassian.jira.plugin.system.project%3Aversions-panel').text

        if not cached:
            req.GET(extract(page, self.patterns['project_avatar']))
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:ajaxhistory/jira.webresources:ajaxhistory.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:browseproject/jira.webresources:browseproject.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=#f0f0f0&bgcolor=#3c78b5')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/newfeature.gif')
            req.GET('/images/icons/box_16.gif')
            req.GET('/s/en_US-64k3hp/664/2/_/images/jira111x30.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')
        

            
    def viewTabIssues(self, cached=False):
        req = self.requests['view']
        
        page = req.GET('/browse/' + self.projectKey + '#selectedTab=com.atlassian.jira.plugin.system.project%3Aissues-panel').text
        
        if not cached:
            req.GET(extract(page, self.patterns['project_avatar']))
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:ajaxhistory/jira.webresources:ajaxhistory.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:browseproject/jira.webresources:browseproject.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/batch.css')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js')
            req.GET('/rest/api/1.0/header-separator?color=#f0f0f0&bgcolor=#3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/icons/newfeature.gif')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/icons/status_reopened.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/s/en_US-64k3hp/664/2/_/images/jira111x30.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/mod_header_bg.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')


    def viewTabComponents(self, cached=False):
        req = self.requests['view']
        
        page = req.GET('/browse/' + self.projectKey + '#selectedTab=com.atlassian.jira.plugin.system.project%3Acomponents-panel').text
        
        if not cached:
            req.GET(extract(page, self.patterns['project_avatar']))
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:ajaxhistory/jira.webresources:ajaxhistory.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/3.1.12/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:browseproject/jira.webresources:browseproject.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/rest/api/1.0/header-separator?color=#f0f0f0&bgcolor=#3c78b5')
            req.GET('/s/en_US-64k3hp/664/2/_/images/jira111x30.png')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/newfeature.gif')
            req.GET('/images/icons/component.gif')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')
    

from HTTPClient import NVPair 
from env import request, extract, valueOrDefault, valueOrEmpty
from java.util.regex import Pattern

class NotificationSchemeAdmin:
    
    def __init__(self, testId):
        self.requests = {
            'browse' : request(testId, 'HTTP-REQ : view notification schemes'),
            'add' : request(testId + 1, 'HTTP-REQ : add notification scheme'),
            'edit' : request(testId + 2, 'HTTP-REQ : edit notification scheme')
        }
        self.patterns = {
            'new_scheme_id' : Pattern.compile('schemeId=([0-9]*)'),
            'edit_name' : Pattern.compile('(?s)name="name".*?value="(.*?)"'), 
            'edit_description' : Pattern.compile('(?s)name="description".*?\>*(.*?)\<'),
            'atl_token' : Pattern.compile('name="atlassian-token" content="(.*?)"')
        }

    def browse(self, cached=False):
        req = self.requests['browse']
        
        page = req.GET('/secure/admin/ViewNotificationSchemes.jspa').text
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avataror/jira.webresources:avataror.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avataror/jira.webresources:avataror.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avatarpicker/jira.webresources:avatarpicker.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:jira-admin/jira.webresources:jira-admin.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js?context=admin')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/batch.css')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/en_US-64k3hp/664/2/_/images/jira111x30.png')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/images/icons/aui-message-icon-sprite.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/aui-formbar-button-inactive-bg.png')
            req.GET('/s/en_US-64k3hp/664/2/1/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-64k3hp/664/2/_/images/icons/ico_help.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')

        return extract(page, self.patterns['atl_token'])
        
        
    def add(self, token, scheme, cached=False):
        req = self.requests['add']
        
        page = req.GET('/secure/admin/AddNotificationScheme!default.jspa?atl_token=' + token).text
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avataror/jira.webresources:avataror.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avataror/jira.webresources:avataror.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avatarpicker/jira.webresources:avatarpicker.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:jira-admin/jira.webresources:jira-admin.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/batch.css')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js?context=admin')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/en_US-64k3hp/664/2/_/images/icons/ico_help.png')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/s/en_US-64k3hp/664/2/_/images/jira111x30.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/aui-formbar-button-inactive-bg.png')
            req.GET('/s/en_US-64k3hp/664/2/1/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')
            
        token = extract(page, self.patterns['atl_token'])
            
        response = req.POST('/secure/admin/AddNotificationScheme.jspa',
        (
            NVPair('atl_token', token),
            NVPair('name', scheme['name']),
            NVPair('description', valueOrEmpty(scheme, 'description')),
            NVPair('Add', 'Add'),
        ), ( 
            NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
        ))
        
        newSchemeId = extract(response.getHeader('Location'), self.patterns['new_scheme_id'])
                 
        req.GET('/secure/admin/EditNotifications!default.jspa?schemeId=' + newSchemeId)
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/bullet_creme.gif')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/images/icons/aui-message-icon-sprite.png')

        return newSchemeId
        
        
    def edit(self, token, scheme, cached=False):
        req = self.requests['edit']
        sid = scheme['id']
        
        page = req.GET('/secure/admin/EditNotificationScheme!default.jspa?atl_token=' + token + '&schemeId=' + sid).text
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avataror/jira.webresources:avataror.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avataror/jira.webresources:avataror.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avatarpicker/jira.webresources:avatarpicker.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:jira-admin/jira.webresources:jira-admin.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/batch.css')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js?context=admin')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/aui-formbar-button-inactive-bg.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/s/en_US-64k3hp/664/2/_/images/jira111x30.png')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/2/1/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/images/toolbar/aui-toolbar-button-active-bg.png')
            req.GET('/secure/admin/ViewNotificationSchemes.jspa')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/images/icons/aui-message-icon-sprite.png')
            req.GET('/s/en_US-64k3hp/664/2/_/images/icons/ico_help.png')
            
        token = extract(page, self.patterns['atl_token'])
            
        req.POST('/secure/admin/EditNotificationScheme.jspa',
        (
            NVPair('atl_token', token),
            NVPair('name', valueOrDefault(scheme, 'name', extract(page, self.patterns['edit_name']))),
            NVPair('description', valueOrDefault(scheme, 'description', extract(page, self.patterns['edit_description']))),
            NVPair('Update', 'Update'),
            NVPair('schemeId', sid),
        ), ( 
            NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
        ))

        self.browse(cached)

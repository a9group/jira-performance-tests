from HTTPClient import NVPair
from env import request, extract, valueOrEmpty, valueOrDefault, extractAll, postMultipart
from java.util.regex import Pattern

class IssueCreator:
    
    def __init__(self, testIndex):
        self.requests = {
            'create' : request(testIndex, 'HTTP-REQ : create issue'),
            'view' : request(testIndex + 1, 'HTTP-REQ : view created issue')
        }
        self.patterns = {
            'project_avatar' : Pattern.compile('id="project-avatar".*?src="(.*?)"'),
            'user_avatars' : Pattern.compile('background-image:url\((\/secure\/useravatar?.*?)\)'),
            'create_response_issuekey' : Pattern.compile('browse\/(.*)'),
            'atl_token' : Pattern.compile('id="atlassian-token".*?content="(.*?)"')
        }        
    
    def create(self, atl_token, projectId, create={}, cached=False):
        req = self.requests['create']

        req.GET('/secure/CreateIssue.jspa?pid=' + projectId + '&issuetype=' + valueOrDefault(create, 'issuetype', '1') + '&Create=Create')
        req.GET('/rest/api/1.0/admin/issuetypeschemes?includeRecent=true&_=1328588320472')

        if not cached:        
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.css')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/3/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/3/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.js')
            req.GET('/s/en_US-64k3hp/664/3/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:jira-fields/jira.webresources:jira-fields.js')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:calendar/jira.webresources:calendar.js')
            req.GET('/s/en_US-64k3hp/664/3/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/s/en_US-64k3hp/664/3/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/s/en_US-64k3hp/664/3/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/s/en_US-64k3hp/664/3/1/_/download/superbatch/css/batch.css')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/header-separator?color=#f0f0f0&bgcolor=#3c78b5')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/en_US-64k3hp/664/3/1/_/download/superbatch/css/images/icons/aui-icon-forms.gif')
            req.GET('/images/icons/priority_blocker.gif')
            req.GET('/images/icons/priority_critical.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/icons/priority_minor.gif')
            req.GET('/images/icons/priority_trivial.gif')
            req.GET('/images/icons/ico_help.png')
            req.GET('/images/icons/filter_public.gif')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-64k3hp/664/3/_/images/jira111x30.png')
            req.GET('/s/en_US-64k3hp/664/3/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')
                
        response = postMultipart(req, '/secure/CreateIssueDetails.jspa',
            (
                NVPair('atl_token', atl_token),
                NVPair('pid', projectId),
                NVPair('issuetype', valueOrDefault(create, 'issuetype', '1')),
                NVPair('summary', valueOrEmpty(create, 'summary')),
                NVPair('priority', valueOrDefault(create, 'priority', '3')),
                NVPair('assignee', valueOrDefault(create, 'assignee', '-1')),
                NVPair('environment', valueOrEmpty(create, 'environment')),
                NVPair('description', valueOrEmpty(create, 'description')),
                NVPair('duedate', valueOrEmpty(create, 'duedate')),
                NVPair('reporter', create['reporter']),
                NVPair('tempFilename', valueOrEmpty(create, 'tempFilename')),
                NVPair('Create', 'Create'),
            ))

        newIssueKey = extract(response.getHeader('Location'), self.patterns['create_response_issuekey'])
        
        req = self.requests['view']
        
        page = req.GET('/browse/' + newIssueKey).text
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:calendar/jira.webresources:calendar.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:viewissue/jira.webresources:viewissue.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:jira-fields/jira.webresources:jira-fields.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/batch.css')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js?context=issueaction&context=issuenavigation')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/s/en_US-64k3hp/664/2/_/images/jira111x30.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/button_bg.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/view_20.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/stalker-std-grad.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/icon_blockexpanded.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/mod_header_bg.png')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/tab_bg.png')
            req.GET('/rest/api/1.0/header-separator?color=#f0f0f0&bgcolor=#3c78b5')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')
            
            req.GET(extract(page, self.patterns['project_avatar'], (('&amp;', '&'), ) ))
            for userAvatar in extractAll(page, self.patterns['user_avatars'], (('&amp;', '&'), )):
                req.GET(userAvatar)
                
        return extract(page, self.patterns['atl_token'])

from HTTPClient import NVPair
from env import request, valueOrEmpty, extract
from java.util.regex import Pattern

class IssueNavigator:
    
    def __init__(self, testIndex):
        self.requests = {
            'simple_search' : request(testIndex, 'HTTP-REQ : issue navigator simple search'),
            'advanced_search' : request(testIndex + 1, 'HTTP-REQ : issue navigator advanced search')
        }
        self.patterns = {
            'atl_token' : Pattern.compile('id="atlassian-token".*?content="(.*?)"')                         
        }
        
    def simpleSearch(self, query={ 'pid' : '-1' }, cached=False):
        req = self.requests['simple_search']
        
        page = req.GET('/secure/IssueNavigator.jspa?mode=show&createNew=true').text
        
        if not cached:
            req.GET('/s/en_USkppxta/712/7/2b1ee4eaa4ba7880e509215b4ec88293/_/download/contextbatch/css/jira.navigator.simple,atl.general/batch.css')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_USkppxta/712/7/2b1ee4eaa4ba7880e509215b4ec88293/_/download/contextbatch/js/jira.navigator.simple,atl.general/batch.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            req.GET('/rest/api/1.0/shortcuts/712/378e85a786f108d2407c8cf432595238/shortcuts.js?context=issueaction&context=issuenavigation')
            req.GET('/includes/jira/issue/searchIssueTypes.js')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/css/batch.css')
            req.GET('/images/icons/ico_help.png')
            req.GET('/images/icons/bug.gif')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/tab_bg.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/icon_filtercollapse.png')
            req.GET('/images/icons/newfeature.gif')
            req.GET('/images/icons/improvement.gif')
            req.GET('/images/icons/task.gif')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/mod_header_bg.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/icon_blockcollapsed.png')
            req.GET('/images/icons/filter_public.gif')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/new/icon18-charlie.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/create_12.png')
            req.GET('/s/en_USkppxta/712/7/_/images/jira111x30.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/icon_blockexpanded.png')
            req.GET('/images/icons/status_open.gif')
            req.GET('/images/icons/status_inprogress.gif')
            req.GET('/images/icons/status_resolved.gif')
            req.GET('/images/icons/status_reopened.gif')
            req.GET('/images/icons/status_closed.gif')
            req.GET('/images/icons/priority_blocker.gif')
            req.GET('/images/icons/priority_critical.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/icons/priority_minor.gif')
            req.GET('/images/icons/priority_trivial.gif')
            req.GET('/s/en_USkppxta/712/7/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-icon-forms.gif')
            req.GET('/s/en_USkppxta/712/7/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-message-icon-sprite.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/permalink_light_16.png')
                
        req.POST('/secure/IssueNavigator.jspa?',
            (
                NVPair('atl_token', extract(page, self.patterns['atl_token'])),
                NVPair('reset', 'update'),
                NVPair('query', valueOrEmpty(query, 'query')),
                NVPair('summary', valueOrEmpty(query, 'summary')),
                NVPair('description', valueOrEmpty(query, 'description')),
                NVPair('body', valueOrEmpty(query, 'body')),
                NVPair('environment', valueOrEmpty(query, 'environment')),
                NVPair('pid', valueOrEmpty(query, 'pid')),
                NVPair('type', valueOrEmpty(query, 'type')),
                NVPair('status', valueOrEmpty(query, 'status')),
                NVPair('resolution', valueOrEmpty(query, 'resolution')),
                NVPair('priority', valueOrEmpty(query, 'priority')),
                NVPair('labels', valueOrEmpty(query, 'labels')),
                NVPair('created:after', valueOrEmpty(query, 'created:after')),
                NVPair('created:before', valueOrEmpty(query, 'created:before')),
                NVPair('created:previous', valueOrEmpty(query, 'created:previous')),
                NVPair('created:next', valueOrEmpty(query, 'created:next')),
                NVPair('updated:after', valueOrEmpty(query, 'updated:after')),
                NVPair('updated:before', valueOrEmpty(query, 'updated:before')),
                NVPair('updated:previous', valueOrEmpty(query, 'updated:previous')),
                NVPair('updated:next', valueOrEmpty(query, 'updated:next')),
                NVPair('duedate:after', valueOrEmpty(query, 'duedate:after')),
                NVPair('duedate:before', valueOrEmpty(query, 'duedate:before')),
                NVPair('duedate:previous', valueOrEmpty(query, 'duedate:previous')),
                NVPair('duedate:next', valueOrEmpty(query, 'duedate:next')),
                NVPair('resolutiondate:after', valueOrEmpty(query, 'resolutiondate:after')),
                NVPair('resolutiondate:before', valueOrEmpty(query, 'resolutiondate:before')),
                NVPair('resolutiondate:previous', valueOrEmpty(query, 'resolutiondate:previous')),
                NVPair('resolutiondate:next', valueOrEmpty(query, 'resolutiondate:next')),
                NVPair('workratio:min', valueOrEmpty(query, 'workratio:min')),
                NVPair('workratio:max', valueOrEmpty(query, 'workratio:max')),
            ), ( 
                NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
            ))

        
    def advancedSearch(self, jql, cached=False):
        req = self.requests['advanced_search']

        req.GET('/secure/IssueNavigator!executeAdvanced.jspa')
        
        if not cached:
            req.GET('/s/en_USkppxta/712/7/2b1ee4eaa4ba7880e509215b4ec88293/_/download/contextbatch/css/jira.navigator.advanced,atl.general/batch.css')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_USkppxta/712/7/2b1ee4eaa4ba7880e509215b4ec88293/_/download/contextbatch/js/jira.navigator.advanced,atl.general/batch.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:jqlautocomplete/jira.webresources:jqlautocomplete.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            req.GET('/rest/api/1.0/shortcuts/712/378e85a786f108d2407c8cf432595238/shortcuts.js?context=issueaction&context=issuenavigation')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/tab_bg.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/mod_header_bg.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/icon_filtercollapse.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/permalink_light_16.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/icon_blockexpanded.png')
            req.GET('/s/en_USkppxta/712/7/_/images/icons/ico_help.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/accept.png')
            req.GET('/s/en_USkppxta/712/7/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-message-icon-sprite.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/new/icon18-charlie.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/create_12.png')
            req.GET('/s/en_USkppxta/712/7/_/images/jira111x30.png')         
                    
        req.POST('/secure/IssueNavigator!executeAdvanced.jspa',
                (                
                    NVPair('jqlQuery', jql),
                    NVPair('runQuery', 'true'),
                    NVPair('autocomplete', 'on'),
                ), ( 
                    NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
                ))

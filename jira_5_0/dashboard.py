from env import request, extract, host
from HTTPClient import NVPair
from java.util.regex import Pattern

class Dashboard:
    
    def __init__(self, testIndex):
        self.requests = {
            'home_not_logged_in' : request(testIndex, 'HTTP-REQ : home not logged in'),
            'login' : request(testIndex + 1, 'HTTP-REQ : perform login'),
            'home_logged_in' : request(testIndex + 2, 'HTTP-REQ : home logged in'),
        }
        self.gadgetRegex = {
            'login' : Pattern.compile('id":"0.*?renderedGadgetUrl":"http:\\\/\\\/.*?(\\\/.*?)"'),
            'intro' : Pattern.compile('title":"Introduction".*?renderedGadgetUrl":"http:\\\/\\\/.*?(\\\/.*?)"'),
            'assignedToMe' : Pattern.compile('title":"Assigned\sto\sMe".*?renderedGadgetUrl":"http:\\\/\\\/.*?(\\\/.*?)"'),
            'favFilters' : Pattern.compile('title":"Favourite\sFilters".*?renderedGadgetUrl":"http:\\\/\\\/.*?(\\\/.*?)"'),
            'admin' : Pattern.compile('title":"Admin".*?renderedGadgetUrl":"http:\\\/\\\/.*?(\\\/.*?)"')
        }
        
    # go to JIRA home and log in
    def login(self, username, password, cached=False):
        self.requests['login'].POST('/rest/gadget/1.0/login', ( 
                                        NVPair('os_username', username), 
                                        NVPair('os_password', password), 
                                        NVPair('os_captcha', ''), 
                                    ), ( 
                                        NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
                                    )) 
        
            
    def loggedIn(self, cached=False):
        req = self.requests['home_logged_in']

        req.GET('/')
        req.GET('/secure/MyJiraHome.jspa')
        dashPage = req.GET('/secure/Dashboard.jspa').text
        if self.gadgetRegex['intro'].matcher(dashPage).find():
            req.GET(extract(dashPage, self.gadgetRegex['intro'], (('\/', '/'), )))
        if self.gadgetRegex['assignedToMe'].matcher(dashPage).find():
            req.GET(extract(dashPage, self.gadgetRegex['assignedToMe'], (('\/', '/'), )))
        if self.gadgetRegex['favFilters'].matcher(dashPage).find():
            req.GET(extract(dashPage, self.gadgetRegex['favFilters'], (('\/', '/'), )))
        req.POST('/plugins/servlet/gadgets/dashboard-diagnostics', ( NVPair('uri', host()+'/secure/Dashboard.jspa'), ), ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), ) )
        req.GET('/rest/gadget/1.0/favfilters?showCounts=true')
        req.GET('/rest/gadget/1.0/intro')
        req.GET('/plugins/servlet/streams?maxResults=5&streams=')        
        req.GET('/rest/activity-stream/1.0/preferences')        
        req.GET('/rest/gadget/1.0/issueTable/jql?jql=assignee+=+currentUser()+AND+resolution+=+unresolved+ORDER+BY+priority+DESC,+created+ASC&num=10&addDefault=true&columnNames=&enableSorting=true&sortBy=null&paging=true&startIndex=0&showActions=true')
        req.GET('/s/en_USdtocgs/710/2/5.0.3/_/download/resources/com.atlassian.streams.streams-jira-plugin:date-en-US/date.js?callback=ActivityStreams.loadDateJs&_=1328243472509')

        if self.gadgetRegex['admin'].matcher(dashPage).find():
            req.GET('/rest/gadget/1.0/admin')
            req.GET(extract(dashPage, self.gadgetRegex['admin'], (('\/', '/'), )))
                
        if not cached:
            req.GET('/s/en_USkppxta/712/4/b62a41cf5d02366c1ccca01a4fdac385/_/download/contextbatch/css/atl.dashboard/batch.css')
            req.GET('/s/en_USkppxta/712/4/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_USkppxta/712/4/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/css/atl.general/batch.css')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.dashboard:dashboard/com.atlassian.gadgets.dashboard:dashboard.css')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.dashboard:gadget-dashboard-resources/com.atlassian.gadgets.dashboard:gadget-dashboard-resources.css')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_USkppxta/712/4/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_USkppxta/712/4/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/js/atl.general/batch.js')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.dashboard:dashboard/com.atlassian.gadgets.dashboard:dashboard.js')
            req.GET('/s/en_USkppxta/712/4/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.dashboard:gadget-dashboard-resources/com.atlassian.gadgets.dashboard:gadget-dashboard-resources.js')
            req.GET('/rest/api/1.0/shortcuts/712/378e85a786f108d2407c8cf432595238/shortcuts.js')
            req.GET('/s/en_USkppxta/712/4/_/images/jira111x30.png')
            req.GET('/s/en_USkppxta/712/4/1.0/_/images/icons/create_12.png')
            req.GET('/s/en_USkppxta/712/4/1.0/_/images/icons/tools_20.png')
            req.GET('/s/en_USkppxta/712/4/1.0/_/images/icons/new/icon18-charlie.png')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/resources/com.atlassian.gadgets.dashboard:dashboard/css/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/batch/jira.webresources:util-lite/jira.webresources:util-lite.css')
            req.GET('/plugins/servlet/gadgets/js/auth-refresh.js?v=61901225c398ca5626e5f70307fcebd&container=atlassian&debug=0')
            req.GET('/s/en_USkppxta/712/4/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs-gadgets-base/com.atlassian.auiplugin:ajs-gadgets-base.css')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets-lite/com.atlassian.gadgets.publisher:ajs-gadgets-lite.css')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajax/com.atlassian.gadgets.publisher:ajax.css')
            req.GET('/s/en_USkppxta/712/4/5.0-rc4/_/download/batch/com.atlassian.jira.gadgets:common-lite/com.atlassian.jira.gadgets:common-lite.css')
            req.GET('/s/en_USkppxta/712/4/3.5.5/_/download/batch/com.atlassian.auiplugin:jquery-lib/com.atlassian.auiplugin:jquery-lib.js')
            req.GET('/s/en_USkppxta/712/4/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs-gadgets-base/com.atlassian.auiplugin:ajs-gadgets-base.js')
            req.GET('/s/en_USkppxta/712/4/3.5.5/_/download/batch/com.atlassian.auiplugin:jquery-compatibility/com.atlassian.auiplugin:jquery-compatibility.js')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:templates/com.atlassian.gadgets.publisher:templates.js')
            req.GET('/s/en_USkppxta/712/4/3.5.5/_/download/batch/com.atlassian.auiplugin:aui-core/com.atlassian.auiplugin:aui-core.js')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:util/com.atlassian.gadgets.publisher:util.js')
            req.GET('/s/en_USkppxta/712/4/5.0-rc4/_/download/batch/com.atlassian.jira.gadgets:common-lite/com.atlassian.jira.gadgets:common-lite.js')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajax/com.atlassian.gadgets.publisher:ajax.js')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets-lite/com.atlassian.gadgets.publisher:ajs-gadgets-lite.js')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/batch/jira.webresources:util-lite/jira.webresources:util-lite.js')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/batch/jira.webresources:inline-layer/jira.webresources:inline-layer.css')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/batch/jira.webresources:issue-table/jira.webresources:issue-table.css')
            req.GET('/s/en_USkppxta/712/4/5.0-rc4/_/download/batch/com.atlassian.jira.gadgets:g-assigned-to-me/com.atlassian.jira.gadgets:g-assigned-to-me.css')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/batch/jira.webresources:content-retrievers/jira.webresources:content-retrievers.js')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/batch/jira.webresources:inline-layer/jira.webresources:inline-layer.js')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/batch/jira.webresources:list/jira.webresources:list.js')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/batch/jira.webresources:dropdown/jira.webresources:dropdown.js')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/batch/jira.webresources:issue-table/jira.webresources:issue-table.js')
            req.GET('/s/en_USkppxta/712/4/5.0-rc4/_/download/batch/com.atlassian.jira.gadgets:g-assigned-to-me/com.atlassian.jira.gadgets:g-assigned-to-me.js')
            req.GET('/s/en_USkppxta/712/4/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/en_USkppxta/712/4/5.0-rc4/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.css')
            req.GET('/s/en_USkppxta/712/4/5.1.0/_/download/batch/com.atlassian.streams.actions:commentActionHandlers/com.atlassian.streams.actions:commentActionHandlers.css')
            req.GET('/s/en_USkppxta/712/4/5.1.0/_/download/batch/com.atlassian.streams:datepicker/com.atlassian.streams:datepicker.css')
            req.GET('/s/en_USkppxta/712/4/5.1.0/_/download/batch/com.atlassian.streams:streamsWebResources/com.atlassian.streams:streamsWebResources.css')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets/com.atlassian.gadgets.publisher:ajs-gadgets.css')
            req.GET('/s/en_USkppxta/712/4/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets/com.atlassian.gadgets.publisher:ajs-gadgets.js')
            req.GET('/s/en_USkppxta/712/4/5.1.0/_/download/batch/com.atlassian.streams:date-default/com.atlassian.streams:date-default.js')
            req.GET('/s/en_USkppxta/712/4/5.0-rc4/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.js')
            req.GET('/s/en_USkppxta/712/4/5.1.0/_/download/batch/com.atlassian.streams:date-js/com.atlassian.streams:date-js.js')
            req.GET('/s/en_USkppxta/712/4/5.1.0/_/download/batch/com.atlassian.streams:streams-parent-js/com.atlassian.streams:streams-parent-js.js')
            req.GET('/s/en_USkppxta/712/4/5.1.0/_/download/batch/com.atlassian.streams:datepicker/com.atlassian.streams:datepicker.js')
            req.GET('/s/en_USkppxta/712/4/5.1.0/_/download/batch/com.atlassian.streams.actions:inlineActionsJs/com.atlassian.streams.actions:inlineActionsJs.js')
            req.GET('/s/en_USkppxta/712/4/5.1.0/_/download/batch/com.atlassian.streams.actions:commentActionHandlers/com.atlassian.streams.actions:commentActionHandlers.js')
            req.GET('/s/en_USkppxta/712/4/5.1.0/_/download/batch/com.atlassian.streams.jira.inlineactions:actionHandlers/com.atlassian.streams.jira.inlineactions:actionHandlers.js')
            req.GET('/s/en_USkppxta/712/4/5.1.0/_/download/batch/com.atlassian.streams:streamsGadgetResources/com.atlassian.streams:streamsGadgetResources.js')
            req.GET('/s/en_USkppxta/712/4/5.1.0/_/download/batch/com.atlassian.streams:streamsWebResources/com.atlassian.streams:streamsWebResources.js')
            req.GET('/s/en_USkppxta/712/4/5.0-rc4/_/download/batch/com.atlassian.jira.gadgets:g-admin/com.atlassian.jira.gadgets:g-admin.js')
            req.GET('/s/en_USkppxta/712/4/5.1.0/_/download/resources/com.atlassian.streams:streamsWebResources/images/throbber.gif')
            req.GET('/s/en_USkppxta/712/4/5.1.0/_/download/resources/com.atlassian.streams:streamsWebResources/images/full-view-icon.png')
            req.GET('/s/en_USkppxta/712/4/5.1.0/_/download/resources/com.atlassian.streams:streamsWebResources/images/feed-icon.png')
            req.GET('/s/en_USkppxta/712/4/5.1.0/_/download/resources/com.atlassian.streams:streamsWebResources/images/gadget-loading.gif')
            req.GET('/s/en_USkppxta/712/4/5.1.0/_/download/resources/com.atlassian.streams:streamsWebResources/images/list-view-icon.png')
            req.GET('/s/en_USkppxta/712/4/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-icon-tools.gif')
            req.GET('/images/64jira.png')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/s/en_USkppxta/712/4/1.0/_/images/icons/icon_descending.png')
            req.GET('/s/en_USkppxta/712/4/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/resources/jira.webresources:global-static/wiki-renderer.css')
            req.GET('/images/icons/bug.gif')
            req.GET('/s/en_USkppxta/712/4/1.0/_/images/icons/sprites/icons_module.png')


    # go to home page, which redirects to dashboard
    def notLoggedIn(self, cached=False):
        req = self.requests['home_not_logged_in']

        req.GET('/')
        req.GET('/secure/MyJiraHome.jspa')
        dashPage = req.GET('/secure/Dashboard.jspa').text

        req.POST('/plugins/servlet/gadgets/dashboard-diagnostics', ( NVPair('uri', host()+'/secure/Dashboard.jspa'), ), ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), ) )
        req.GET(extract(dashPage, self.gadgetRegex['intro'], (('\/', '/'), )))
        req.GET(extract(dashPage, self.gadgetRegex['login'], (('\/', '/'), )))
        req.GET('/rest/gadget/1.0/intro')
        
        if not cached:
            req.GET('/s/en_USkppxta/712/4/b62a41cf5d02366c1ccca01a4fdac385/_/download/contextbatch/css/atl.dashboard/batch.css')
            req.GET('/s/en_USkppxta/712/4/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/css/atl.general/batch.css')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.dashboard:dashboard/com.atlassian.gadgets.dashboard:dashboard.css')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.dashboard:gadget-dashboard-resources/com.atlassian.gadgets.dashboard:gadget-dashboard-resources.css')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_USkppxta/712/4/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_USkppxta/712/4/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.dashboard:dashboard/com.atlassian.gadgets.dashboard:dashboard.js')
            req.GET('/s/en_USkppxta/712/4/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/js/atl.general/batch.js')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/en_USkppxta/712/4/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.dashboard:gadget-dashboard-resources/com.atlassian.gadgets.dashboard:gadget-dashboard-resources.js')
            req.GET('/rest/api/1.0/shortcuts/712/378e85a786f108d2407c8cf432595238/shortcuts.js')
            req.GET('/s/en_USkppxta/712/4/_/images/jira111x30.png')
            req.GET('/s/en_USkppxta/712/4/1.0/_/images/icons/new/icon18-charlie.png')
            req.GET('/plugins/servlet/gadgets/js/auth-refresh.js?v=61901225c398ca5626e5f70307fcebd&container=atlassian&debug=0')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajax/com.atlassian.gadgets.publisher:ajax.css')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets-lite/com.atlassian.gadgets.publisher:ajs-gadgets-lite.css')
            req.GET('/s/en_USkppxta/712/4/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs-gadgets-base/com.atlassian.auiplugin:ajs-gadgets-base.css')
            req.GET('/s/en_USkppxta/712/4/3.5.5/_/download/batch/com.atlassian.auiplugin:jquery-lib/com.atlassian.auiplugin:jquery-lib.js')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/batch/jira.webresources:util-lite/jira.webresources:util-lite.css')
            req.GET('/s/en_USkppxta/712/4/3.5.5/_/download/batch/com.atlassian.auiplugin:jquery-compatibility/com.atlassian.auiplugin:jquery-compatibility.js')
            req.GET('/s/en_USkppxta/712/4/5.0-rc4/_/download/batch/com.atlassian.jira.gadgets:common-lite/com.atlassian.jira.gadgets:common-lite.css')
            req.GET('/s/en_USkppxta/712/4/3.5.5/_/download/batch/com.atlassian.auiplugin:aui-core/com.atlassian.auiplugin:aui-core.js')
            req.GET('/s/en_USkppxta/712/4/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs-gadgets-base/com.atlassian.auiplugin:ajs-gadgets-base.js')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:util/com.atlassian.gadgets.publisher:util.js')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajax/com.atlassian.gadgets.publisher:ajax.js')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets-lite/com.atlassian.gadgets.publisher:ajs-gadgets-lite.js')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/batch/jira.webresources:util-lite/jira.webresources:util-lite.js')
            req.GET('/s/en_USkppxta/712/4/5.0-rc4/_/download/batch/com.atlassian.jira.gadgets:common-lite/com.atlassian.jira.gadgets:common-lite.js')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:templates/com.atlassian.gadgets.publisher:templates.js')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/batch/jira.webresources:captcha/jira.webresources:captcha.js')
            req.GET('/s/en_USkppxta/712/4/1.0/_/download/batch/jira.webresources:captcha/jira.webresources:captcha.css')
            req.GET('/s/en_USkppxta/712/4/5.0-rc4/_/download/batch/com.atlassian.jira.gadgets:g-login/com.atlassian.jira.gadgets:g-login.js')
            req.GET('/s/en_USkppxta/712/4/3.1.21/_/download/resources/com.atlassian.gadgets.publisher:ajs-gadgets-lite/images/loading.gif')
            req.GET('/images/64jira.png')

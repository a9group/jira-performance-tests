from HTTPClient import NVPair
from env import request, extract, valueOrEmpty, valueOrDefault, extractAll, randChoice
from java.util.regex import Pattern
from net.grinder.script.Grinder import grinder

class IssueCreator:
    
    def __init__(self, testIndex):
        self.requests = {
            'create' : request(testIndex, 'HTTP-REQ : create issue')
        }
        self.patterns = {
            'project_avatars' : Pattern.compile('background-image:url\((\/secure\/projectavatar.*?)\)'),
            'user_avatars' : Pattern.compile('background-image:url\((\/secure\/useravatar.*?)\)'),
            'create_response_issuekey' : Pattern.compile('"issueKey":"(.*?)"'),
            'atl_token' : Pattern.compile('"atl_token":"(.*?)"'),
            'options' : Pattern.compile('<option.*?value=\\\\"(.*?)\\\\"'),
        }

    def extractOptions(self, select):
        p = self.patterns['options']
        m = p.matcher(select)
        options = []
        while(m.find()):
            options.append(m.group(1))
        return options

    def extractOptGroup(self, dialog, projectId):
        selection='<input.*?title=\\\\"'+projectId+'.*?value=\\\\"(.*?)\\\\"'
        schemeId = extract(dialog,Pattern.compile(selection))
        selection='data-scheme-id=\\\\"'+schemeId+'(.*?)<\/optgroup'
        p=Pattern.compile(selection)
        return extract(dialog, p)
       
    def create(self, projectId, create={}, cached=False):
        req = self.requests['create']

        dialog = req.POST('/secure/QuickCreateIssue!default.jspa?decorator=none', ( NVPair('Content-Type', 'application/json'), ) ).text
        
        if not cached:
            req.GET('/images/icons/filter_public.gif')
            req.GET('/images/icons/ico_help.png')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/newfeature.gif')
            req.GET('/s/en_USkppxta/712/7/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs/images/toolbar/aui-toolbar-button-inactive-bg.png')
            req.GET('/s/en_USkppxta/712/7/1.0.28/_/download/resources/com.atlassian.jira.jira-quick-edit-plugin:quick-form/images/icon-cog.png')
            req.GET('/s/en_USkppxta/712/7/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-icon-forms.gif')
            req.GET('/images/icons/improvement.gif')
            req.GET('/images/icons/task.gif')

            for userAvatar in extractAll(dialog, self.patterns['user_avatars'], (('&amp;', '&'), )):
                req.GET(userAvatar)
            for projectAvatar in extractAll(dialog, self.patterns['project_avatars'], (('&amp;', '&'), )):
                req.GET(projectAvatar)
            req.GET('/images/icons/priority_blocker.gif')
            req.GET('/images/icons/priority_minor.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/icons/priority_critical.gif')
            req.GET('/images/icons/priority_trivial.gif')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/wait.gif')
            req.GET('/s/en_USkppxta/712/7/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-message-icon-sprite.png')
            
        req.POST('/rest/quickedit/1.0/userpreferences/create', 
                 '{\"useQuickForm\":false,\"fields\":[\"summary\",\"description\",\"priority\",\"versions\",\"components\"],\"showWelcomeScreen\":true}', 
                 ( NVPair('Content-Type', 'application/json'), )
        )
        
        atl_token = extract(dialog, self.patterns['atl_token'])
        selects = self.extractOptGroup(dialog, projectId)

        options=self.extractOptions(selects)

        def_issuetype = randChoice(options)
        response = req.POST('/secure/QuickCreateIssue.jspa?decorator=none',
        (
            NVPair('pid', projectId),
            NVPair('atl_token', atl_token),
            NVPair('issuetype', valueOrDefault(create, 'issuetype', def_issuetype)),
            NVPair('summary', valueOrEmpty(create, 'summary')),
            NVPair('priority', valueOrDefault(create, 'priority', '3')),
            NVPair('assignee', valueOrDefault(create, 'assignee', '-1')),
            NVPair('reporter', create['reporter']),
            NVPair('description', valueOrEmpty(create, 'description')),
            NVPair('environment', valueOrEmpty(create, 'environment')),
            NVPair('duedate', ''),
            NVPair('fieldsToRetain', 'project'),
            NVPair('fieldsToRetain', 'issuetype'),
            NVPair('fieldsToRetain', 'priority'),
            NVPair('fieldsToRetain', 'duedate'),
            NVPair('fieldsToRetain', 'components'),
            NVPair('fieldsToRetain', 'versions'),
            NVPair('fieldsToRetain', 'fixVersions'),
            NVPair('fieldsToRetain', 'assignee'),
            NVPair('fieldsToRetain', 'reporter'),
            NVPair('fieldsToRetain', 'environment'),
            NVPair('fieldsToRetain', 'description'),
            NVPair('fieldsToRetain', 'attachment'),
            NVPair('fieldsToRetain', 'labels'),
        ), ( 
            NVPair('contentType', 'application/x-www-form-urlencoded'), 
        )).text
        
        return extract(response, self.patterns['create_response_issuekey'])
